<?php namespace Mango\Dependable;

interface Dependable {

    /**
     * Returns the unique slug for the item
     *
     * @return mixed
     */
    public function getSlug();

    /**
     * Returns an array of the item's dependencies
     *
     * @return mixed
     */
    public function getDependencies();
}